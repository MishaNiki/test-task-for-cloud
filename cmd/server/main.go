package main

import (
	"flag"
	"gitlab.com/MishaNiki/test-task-for-cloud/internal/app/journal"
	"gitlab.com/MishaNiki/test-task-for-cloud/internal/app/logger"
	"gitlab.com/MishaNiki/test-task-for-cloud/internal/app/tcpserver"
	"log"
)

var configPath string


func init () {
	flag.StringVar(&configPath, "config", "./config/server.json", "path to config")
}


func main() {
	flag.Parse()
	config := tcpserver.NewConfig()
	if err := config.ParseJsonFile(configPath); err != nil {
		log.Fatal(err)
	}
	l, err := logger.ConfigureLogger(config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	jour, err := journal.NewJournal(l)
	if err != nil {
		log.Fatal(err)
	}
	server, err := tcpserver.NewTCPServer(config, jour, l)
	if err != nil {
		log.Fatal(err)
	}
	if err := server.Run(); err != nil {
		log.Fatal(err)
	}
}