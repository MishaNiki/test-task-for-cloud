package main

import (
	"flag"
	"gitlab.com/MishaNiki/test-task-for-cloud/internal/app/logger"
	"gitlab.com/MishaNiki/test-task-for-cloud/internal/app/tcpclient"
	"log"
)

var (
	configPath string
	packageNumber int
)

func init () {
	flag.StringVar(&configPath, "config", "./config/client.json", "path to config")
	flag.IntVar(&packageNumber, "amount", 10, "the number of packets to be sent to the server")
}


func main() {
	flag.Parse()
	config := tcpclient.NewConfig()
	if err := config.ParseJsonFile(configPath); err != nil {
		log.Fatal(err)
	}
	l, err := logger.ConfigureLogger(config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	client, err := tcpclient.NewClient(config, l)
	if err != nil {
		log.Fatal(err)
	}
	if err := client.SendMessages(packageNumber); err != nil {
		log.Fatal(err)
	}
	if err := client.Close(); err != nil {
		log.Fatal(err)
	}
}