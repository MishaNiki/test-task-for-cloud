package msggen

import (
	"crypto/rand"
)

// Generate - generate message
func Generate(size int) ([]byte, error)  {
	b := make([]byte, size)
	_, err := rand.Read(b)
	if err != nil {
		 return nil, err
	}
	return b, nil
}
