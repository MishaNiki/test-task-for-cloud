package models

import (
	"encoding/binary"
	"errors"
)

type ResponseStatus byte

const (
	// MessageRecorded - статус удачной записи
	MessageRecorded ResponseStatus = iota + 1
	// ErrRecordingInJournal - ошибка во время записи
	ErrRecordingInJournal
)

func (r ResponseStatus) Byte() byte {
	return byte(r)
}

type Response struct {
	Status ResponseStatus
	ID     uint64
}

func (r Response) Encode() []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, r.ID)
	return append([]byte{r.Status.Byte()}, b...)
}

// Decode - декодирование
// 	r, err := new(Response).Decode(b)
func (r *Response) Decode(b []byte) (*Response, error) {
	if r == nil {
		return nil, errors.New("nil pointer")
	}
	if len(b) < 9 {
		return nil, errors.New("not valid data")
	}
	r.Status = ResponseStatus(b[0])
	r.ID = binary.BigEndian.Uint64(b[1:])
	return r, nil
}
