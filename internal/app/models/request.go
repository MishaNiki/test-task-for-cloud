package models

import (
	"encoding/binary"
	"math"

	"gitlab.com/MishaNiki/test-task-for-cloud/internal/pkg/msggen"
)

// RequestSize - размер сообщения с данными
const RequestSize = 4 * 1024
const Uint64SizeByte = 8

// Request - транспортная модель пакета. Есть один заголовок, который id отвечает за индентификатор
// 	пакета с данными на клиенте. При этом последний пакет отправленный клиентом должен быть с id равным math.MaxUint64
type Request struct {
	ID   uint64
	Body []byte
}

// Decode - декодирование пакета с данными в модель. Запись заголовка
func (r *Request) Decode(b []byte) (*Request, error) {
	var id uint64
	if len(b) >= 8 {
		id = binary.BigEndian.Uint64(b)
	}
	return &Request{
		ID:   id,
		Body: b,
	}, nil
}

// IsLast - проверка на последний пакет. Для перехода из режима чтения в режим записи в сокет
func (r Request) IsLast() bool {
	return r.ID == math.MaxUint64
}

// NewRequest - создание нового пакета c данными
func NewRequest(id uint64) ([]byte, error) {
	body, err := msggen.Generate(RequestSize - Uint64SizeByte)
	if err != nil {
		return nil, err
	}
	b := make([]byte, Uint64SizeByte, RequestSize)
	binary.BigEndian.PutUint64(b, id)
	return append(b, body...), nil
}

// NewLastRequest - создание крайнего пакета
func NewLastRequest() ([]byte, error) {
	body, err := msggen.Generate(RequestSize - Uint64SizeByte)
	if err != nil {
		return nil, err
	}
	b := make([]byte, 8, RequestSize)
	binary.BigEndian.PutUint64(b, math.MaxUint64)
	return append(b, body...), nil
}
