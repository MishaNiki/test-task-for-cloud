package journal

import (
	"bufio"
	"os"
	"sync"

	"github.com/sirupsen/logrus"
)

type Journal interface {
	Write([]byte) (int, error)
	Close() error
}

type journal struct {
	file *os.File
	mu   *sync.Mutex
	l    *logrus.Logger
}

const fileName = "server_journal"

func NewJournal(logger *logrus.Logger) (Journal, error) {
	var file *os.File
	if _, err := os.Stat(fileName); err == nil {
		if file, err = os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend); err != nil {
			return nil, err
		}
	} else if os.IsNotExist(err) {
		if file, err = os.Create(fileName); err != nil {
			return nil, err
		}
	} else if err != nil {
		return nil, err
	}
	return &journal{
		file: file,
		mu:   &sync.Mutex{},
		l:    logger,
	}, nil
}

func (j *journal) Close() error {
	return j.file.Close()
}

func (j *journal) Write(buf []byte) (n int, err error) {
	j.mu.Lock()
	defer j.mu.Unlock()
	w := bufio.NewWriterSize(j.file, len(buf))
	if _, err = w.Write(buf); err != nil {
		j.l.Debugf("Write:Write(): %s", err.Error())
		return -1, err
	}
	if err = w.Flush(); err != nil {
		j.l.Debugf("Write:Flush(): %s", err.Error())
		return -1, err
	}
	if err = j.file.Sync(); err != nil {
		j.l.Debugf("Write:Sync(): %s", err.Error())
		return -1, err
	}
	return len(buf), nil
}
