package tcpserver

import (
	"io"
	"net"
	"sync"

	"gitlab.com/MishaNiki/test-task-for-cloud/internal/app/models"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// handlerConn - обработчик соединений
type handlerConn struct {
	wg            *sync.WaitGroup
	conn          net.Conn
	journalWriter io.Writer
	l             *logrus.Logger
	requests      chan *models.Request
	responses     chan *models.Response
	buffersPool   *sync.Pool
}

func newHandlerConn(
	conn net.Conn,
	jw io.Writer,
	l *logrus.Logger,
	group *sync.WaitGroup,
) *handlerConn {
	return &handlerConn{
		wg:            group,
		conn:          conn,
		journalWriter: jw,
		l:             l,
		requests:      make(chan *models.Request, 256),
		responses:     make(chan *models.Response, 256),
		buffersPool: &sync.Pool{
			New: func() interface{} { return make([]byte, models.RequestSize) },
		},
	}
}

func (hc *handlerConn) Process() {
	defer hc.wg.Done()
	defer func() {
		err := hc.conn.Close()
		if err != nil {
			hc.l.Error("error closing connection: ", err)
		}
	}()

	wg := sync.WaitGroup{}
	wg.Add(2)
	go hc.RecordProcess(&wg)
	go hc.WriteProcess(&wg)

	for {
		buffer := hc.buffersPool.Get().([]byte)
		n, err := io.ReadFull(hc.conn, buffer)
		if errors.Is(err, io.EOF) {
			hc.l.Debug("client finished sending data")
			break
		}
		hc.l.Debugf("successful receipt count byte %d of the message: %.16x", n, buffer)
		request, err := new(models.Request).Decode(buffer)
		if err != nil {
			continue
		}
		hc.requests <- request
		hc.l.Debugf("successful receipt count byte %d of the message: %v, %v", n, request.ID, buffer[0:8])
		if request.IsLast() {
			break
		}
		hc.buffersPool.Put(buffer)
	}
	close(hc.requests)
	wg.Wait()
}

// RecordProcess - метод процесса записи в файл
func (hc *handlerConn) RecordProcess(wg *sync.WaitGroup) {
	defer wg.Done()
	for request := range hc.requests {
		_, err := hc.journalWriter.Write(request.Body)
		if err != nil {
			hc.responses <- &models.Response{Status: models.ErrRecordingInJournal}
			continue
		}
		hc.l.Debugf("message saved successfully, id: %v", request.ID)
		hc.responses <- &models.Response{Status: models.MessageRecorded, ID: request.ID}
	}
	close(hc.responses)
}

func (hc *handlerConn) WriteProcess(wg *sync.WaitGroup) {
	defer wg.Done()
	for response := range hc.responses {
		_, err := hc.conn.Write(response.Encode())
		if err != nil {
			hc.l.Error("error sending the response: ", err)
			break
		}
	}
}
