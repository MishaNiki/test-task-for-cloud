package tcpserver

import (
	"net"
	"os"
	"os/signal"
	"sync"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/MishaNiki/test-task-for-cloud/internal/app/journal"
)

type TCPServer struct {
	config   Config
	journal  journal.Journal
	listener net.Listener
	l        *logrus.Logger
}

func NewTCPServer(config Config, journal journal.Journal, logger *logrus.Logger) (*TCPServer, error) {
	listener, err := net.Listen("tcp", config.BindAddr)
	if err != nil {
		return nil, errors.Wrap(err, "listener initialization error")
	}
	return &TCPServer{
		listener: listener,
		config:   config,
		journal:  journal,
		l:        logger,
	}, nil
}

func (s *TCPServer) Run() error {
	s.l.Infof("run tcp service: %s\n", s.config.BindAddr)

	sigint := make(chan os.Signal, 1)
	go func() {
		signal.Notify(sigint, os.Interrupt)
		<-sigint
		_ = s.listener.Close()
	}()

	// группа ожидания завершения всех операций загрузки
	wgConns := sync.WaitGroup{}
	for {
		conn, err := s.listener.Accept()
		if errors.Is(err, net.ErrClosed) {
			s.l.Debug("listener successfully completed the job")
			break
		} else if err != nil {
			s.l.Error("listener error: ", err)
			break
		}
		hc := newHandlerConn(conn, s.journal, s.l, &wgConns)
		wgConns.Add(1)
		go hc.Process()
	}
	wgConns.Wait()
	return s.journal.Close()
}
