package tcpclient

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	BindAddr string `json:"bindAddr"`
	LogLevel string `json:"logLevel"`
 }

func NewConfig() Config {
	return Config{
		BindAddr: "127.0.0.1:4543",
		LogLevel: "debug",
	}
}

func (c *Config) ParseJsonFile(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, c)
}
