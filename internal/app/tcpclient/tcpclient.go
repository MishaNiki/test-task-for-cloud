package tcpclient

import (
	"io"
	"math"
	"net"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/MishaNiki/test-task-for-cloud/internal/app/models"
)

type Client struct {
	conn net.Conn
	l    *logrus.Logger
}

func NewClient(config Config, logger *logrus.Logger) (*Client, error) {
	conn, err := net.Dial("tcp", config.BindAddr)
	if err != nil {
		return nil, err
	}
	return &Client{
		conn: conn,
		l:    logger,
	}, nil
}

// SendMessages - отправка сообщений
func (c *Client) SendMessages(amountMessages int) error {
	var err error
	data, err := prepareData(amountMessages)
	if err != nil {
		c.l.Error("errors generate data: ", err)
		return err
	}
	am := uint64(amountMessages)
	for i := uint64(0); i < am; i++ {
		var req []byte
		if i == am-1 {
			req = data[math.MaxUint64]
		} else {
			req = data[i]
		}
		_, err := c.conn.Write(req)
		if err != nil {
			c.l.Error("error sending message: ", err)
			return err
		}
		c.l.Debugf("successfully sending message: %.16x", req)
	}

	for len(data) > 0 {
		response := make([]byte, 9)
		_, err := io.ReadFull(c.conn, response)
		if err != nil && !errors.Is(err, io.EOF) {
			c.l.Error("error receiving message: ", err)
			return err
		}
		resp, err := new(models.Response).Decode(response)
		if err != nil {
			c.l.Error("error decode response: ", err)
			return err
		}
		if resp.Status == models.MessageRecorded {
			c.l.Debugf("msg: %d, successfully delivered", resp.ID)
			delete(data, resp.ID)
		} else if resp.Status == models.ErrRecordingInJournal {
			c.l.Error("server internal error")
			return errors.New("server internal error")
		}
	}
	return nil
}

func (c *Client) Close() error {
	return c.conn.Close()
}

// prepareData - подготовка мапы с данными для отправки
func prepareData(amountMessages int) (map[uint64][]byte, error) {
	var err error
	m := make(map[uint64][]byte, amountMessages)
	if amountMessages > 1 {
		for i := 0; i < amountMessages-1; i++ {
			m[uint64(i)], err = models.NewRequest(uint64(i))
			if err != nil {
				return nil, err
			}
		}
	}
	m[math.MaxUint64], err = models.NewLastRequest()
	if err != nil {
		return nil, err
	}

	return m, nil
}
